package com.example.demo.result;

/**
 * @author Mengxue
 * @date 2020/9/15 11:44
 * @version:
 * @Description:
 */
public class Result {
    //响应码
    private int code;

    public Result(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
