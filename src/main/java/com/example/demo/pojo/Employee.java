package com.example.demo.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

/**
 * @author Mengxue
 * @date 2020/9/16 14:29
 * @version:
 * @Description:
 */
@Entity
@Table(name = "employee")
@JsonIgnoreProperties({"handler","hibernateLazyInitializer"})
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    private String name;

    @ManyToOne
    @JoinColumn(name="sid")
    private Sex sex;
    private String birth;
    private String phone;
    @ManyToOne
    @JoinColumn(name="did")
    private Department department;
    @ManyToOne
    @JoinColumn(name="pid")
    private Position position;
    private String email;
    private String photo;

    public Employee() {

    }

    public Employee(String name, Sex sex, String birth, String phone, Department department, Position position, String email, String photo) {
        this.name = name;
        this.sex = sex;
        this.birth = birth;
        this.phone = phone;
        this.department = department;
        this.position = position;
        this.email = email;
        this.photo = photo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
