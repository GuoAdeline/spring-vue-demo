package com.example.demo.service;

import com.example.demo.Mapper.EmployeeMapper;
import com.example.demo.pojo.Department;
import com.example.demo.pojo.Employee;
import com.example.demo.pojo.Position;
import com.example.demo.pojo.Sex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Mengxue
 * @date 2020/9/20 10:34
 * @version:
 * @Description:
 */
@Service
public class EmployeeService {
    @Autowired
    EmployeeMapper employeeMapper;
    @Autowired
    SexService sexService;
    @Autowired
    DepartmentService departmentService;
    @Autowired
    PositionService positionService;

    public List<Employee> list() {
        return employeeMapper.findAll(Sort.by(Sort.Direction.ASC, "id"));
    }

    public void addOrUpdate(Employee employee) {
        employeeMapper.save(employee);
    }

    public void deleteById(int id) {
        employeeMapper.deleteById(id);
    }

    public List<Employee> listBySex(int sid) {
        Sex sex = sexService.get(sid);
        return employeeMapper.findAllBySex(sex);
    }

    public List<Employee> listByDepartment(int did) {
        Department department = departmentService.get(did);
        return employeeMapper.findAllByDepartment(department);
    }

    public List<Employee> listByPosition(int pid) {
        Position position = positionService.get(pid);
        return employeeMapper.findAllByPosition(position);
    }

//    public List<Employee> Search(String keywords) {
//        return employeeMapper.findAllByNameLikeOrPhoneLike('%' + keywords + '%', '%' + keywords + '%');
//
//    }
    public List<Employee> Search(String keywords) {
        return employeeMapper.findAllByNameLike('%' + keywords + '%');
    }
}
