package com.example.demo.service;

import com.example.demo.Mapper.UserMapper;
import com.example.demo.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Mengxue
 * @date 2020/9/15 13:35
 * @version:
 * @Description:
 */
@Service
public class UserService {
    @Autowired
    UserMapper userMapper;

    public boolean isExist(String username) {
        User user = getByName(username);
        return null!=user;
    }

    public User getByName(String username) {
        return userMapper.findByUsername(username);
    }

    public User get(String username, String password) {
        return userMapper.getByUsernameAndPassword(username, password);
    }

    public void add(User user) {
        userMapper.save(user);
    }
}
