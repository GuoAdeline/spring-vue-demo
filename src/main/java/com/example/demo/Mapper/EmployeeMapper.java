package com.example.demo.Mapper;

import com.example.demo.pojo.Department;
import com.example.demo.pojo.Employee;
import com.example.demo.pojo.Position;
import com.example.demo.pojo.Sex;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Mengxue
 * @date 2020/9/20 9:50
 * @version:
 * @Description:
 */
public interface EmployeeMapper extends JpaRepository<Employee, Integer> {

    List<Employee> findAllBySex(Sex sex);
    List<Employee> findAllByDepartment(Department department);
    List<Employee> findAllByPosition(Position position);
    List<Employee> findAllByNameLikeOrPhoneLike(String keyword1, String keyword2);
    List<Employee> findAllByNameLike(String keyword);
}
