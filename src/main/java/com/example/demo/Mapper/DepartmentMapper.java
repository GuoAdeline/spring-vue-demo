package com.example.demo.Mapper;

import com.example.demo.pojo.Department;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Mengxue
 * @date 2020/9/20 10:03
 * @version:
 * @Description:
 */
public interface DepartmentMapper extends JpaRepository<Department, Integer> {
}
