package com.example.demo.Mapper;

import com.example.demo.pojo.Sex;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Mengxue
 * @date 2020/9/20 10:19
 * @version:
 * @Description:
 */
public interface SexMapper extends JpaRepository<Sex, Integer> {
}
