package com.example.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.pojo.Employee;
import com.example.demo.service.EmployeeService;
import com.example.demo.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author Mengxue
 * @date 2020/9/14 17:09
 * @version:
 * @Description:
 */
@RestController
public class EmployeeController {
    @Autowired
    EmployeeService employeeService;

    @CrossOrigin
    @GetMapping("/api/employee")
    public List<Employee> list() throws Exception {
        return employeeService.list();
    }

    @CrossOrigin
    @PostMapping("/api/employee")
    public Employee addOrUpdate(@RequestBody Employee employee) throws Exception {
        employeeService.addOrUpdate(employee);
        return employee;
    }

    @CrossOrigin
    @PostMapping("/api/delete")
    public void delete(@RequestBody Employee employee) throws Exception {
        employeeService.deleteById(employee.getId());
    }

    @CrossOrigin
    @GetMapping("/api/department/{did}/employee")
    public List<Employee> listByDepartment(@PathVariable("did") int did) throws  Exception {
        if (0 != did) {
            return employeeService.listByDepartment(did);
        } else {
            return list();
        }
    }

    @CrossOrigin
    @GetMapping("/api/sex/{sid}/employee")
    public List<Employee> listBySex(@PathVariable("sid") int sid) throws  Exception {
        if (0 != sid) {
            return employeeService.listBySex(sid);
        } else {
            return list();
        }
    }

    @CrossOrigin
    @GetMapping("/api/position/{pid}/employee")
    public List<Employee> listByPosition(@PathVariable("pid") int pid) throws  Exception {
        if (0 != pid) {
            return employeeService.listByPosition(pid);
        } else {
            return list();
        }
    }

    @CrossOrigin
    @GetMapping("/api/search")
    public List<Employee> searchResult(@RequestParam("keywords") String keywords) {
        // 关键词为空时查询出所有员工列表
        if ("".equals(keywords)) {
            return employeeService.list();
        } else {
            return employeeService.Search(keywords);
        }
    }

//    @CrossOrigin
//    @GetMapping("/api/search")
//    public List<Employee> searchResult(@RequestBody JSONObject keywords) {
//        System.out.println(keywords.getString("keywords"));
//        // 关键词为空时查询出所有员工列表
//        String key = keywords.getString("keywords");、
//        if ("".equals(key)) {
//            return employeeService.list();
//        } else {
//            return employeeService.Search(key);
//        }
//    }

    @CrossOrigin
    @PostMapping("/api/photo")
    public String photoUpload(MultipartFile file) throws Exception {
        String folder = "E:/Projects/vue-springboot-front_backend/img";
        File imageFolder = new File(folder);
        File f = new File(imageFolder, StringUtils.getRandomString(6) + file.getOriginalFilename()
                .substring(file.getOriginalFilename().length() - 4));
        if (!f.getParentFile().exists())
            f.getParentFile().mkdir();
        try {
            file.transferTo(f);
            String imgURL = "http://localhost:8443/api/file/" + f.getName();
            return imgURL;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }

    }





}
