-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `sex`;
CREATE TABLE `sex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `value` varchar(255) NOT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `posit`;
CREATE TABLE `posit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `sid` int(11) DEFAULT NULL,
  `birth` varchar(255) DEFAULT '',
  `phone` varchar(255) DEFAULT '',
  `did` int(11) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT '',
  `photo` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `sex_on_sid` (`sid`),
  CONSTRAINT `sex_on_sid` FOREIGN KEY (`sid`) REFERENCES `sex` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  KEY `department_on_did` (`did`),
  CONSTRAINT `department_on_did` FOREIGN KEY (`did`) REFERENCES `department` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  KEY `position_on_pid` (`pid`),
  CONSTRAINT `position_on_pid` FOREIGN KEY (`pid`) REFERENCES `posit` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Records of tables
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', '123');

INSERT INTO sex (value) VALUES
('男'),
('女');

INSERT INTO department (value) VALUES
('研发中心'),
('质量管理部'),
('系统集成部'),
('运行管理中心');

INSERT INTO posit (value) VALUES
('软件开发工程师'),
('测试工程师'),
('系统集成工程师'),
('运维工程师');

INSERT INTO employee (name, sid, birth, phone, did, pid, email, photo) VALUES
('张三', 1, '1989-10-20', '18923332223', 1, 1, '12345@163.com', 'http://localhost:8443/api/file/6azrsk.jpg'),
('李四', 2, '1990-12-12', '19999999999', 2, 2, 'email@avic-digital.com', 'http://localhost:8443/api/file/p1tyor.jpg');
